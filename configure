#! /usr/bin/env python3
"""\
configure - configure a project following the config.mk convention

Usage: configure [-d] [-f makefile] [-o configfile] [-t] [-u] [--enable=OPT...] [--help]

For each option, the default value is shown in square brackets after 
the explanation.

Gemeral options:

  --help                  Display this usage summary

  -d                      Bump debug level
  -f FILE                 Read tests from specified FILE [Makefile]
  -o FILE                 Write configuration to FILE [config.mk]
  -t                      Test mode.  Mainly of interet tio developers
  -u                      White -U settings for unset macroa

  --enable=OPT            Enable the sepecified option switch. Note that
                          this syntax is slightly different from the historical
                          autotools enable options.
  --srcdir=DIR            find the sources in DIR [configure dir]

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
			  [/usr/local]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
			  [PREFIX]

By default, `make install' will install all the files in
`/usr/local/bin', `/usr/local/lib' etc.  You can specify
an installation prefix other than `/usr/local' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

  --bindir=DIR           user executables [EPREFIX/bin]
  --sbindir=DIR          system admin executables [EPREFIX/sbin]
  --libexecdir=DIR       program executables [EPREFIX/libexec]
  --sysconfdir=DIR       read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR   modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR    modifiable single-machine data [PREFIX/var]
  --libdir=DIR           object code libraries [EPREFIX/lib]
  --includedir=DIR       C header files [PREFIX/include]
  --oldincludedir=DIR    C header files for non-gcc [/usr/include]
  --datarootdir=DIR      read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR          read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR          info documentation [DATAROOTDIR/info]
  --localedir=DIR        locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR           man documentation [DATAROOTDIR/man]
  --docdir=DIR           documentation root [DATAROOTDIR/doc/giflib]
  --htmldir=DIR          html documentation [DOCDIR]
  --dvidir=DIR           dvi documentation [DOCDIR]
  --pdfdir=DIR           pdf documentation [DOCDIR]
  --psdir=DIR            ps documentation [DOCDIR]

Requires Python 3.5 or more recent
"""
# This code is intended to be embedded in your project. The author
# grants permission for it to be distributed under the prevailing
# license of your project if you choose, provided that license is
# OSD-compliant; otherwise the following SPDX tag incorporates the
# MIT No Attribution license by reference.
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: MIT-0
import getopt
import os
import re

import platform
import shlex
import shutil
import string
import subprocess
import sys
import tempfile

# pylint: disable=invalid-name,redefined-outer-name,fixme,global-statement,too-many-statements,exec-used

ECHO_DEBUG = 1
COMMAND_DEBUG = 2
COMPILE_DEBUG = 3
TRACE_DEBUG = 3

compile_command = os.environ.get("CC", "cc") + " "
source_extension = ".cpp" if compile_command in ("g++", "cxx") else ".c"
for option in ("CXX", "CFLAGS", "CPPFLAGS", "CXXFLAGS", "LDFLAGS"):
    if option in os.environ:
        compile_command += os.environ[option] + " "

##
## Functions safe for use in scripts begin here
##

# pylint: disable=too-many-arguments,too-many-statements,too-many-locals
def testcompile(source, capture=False):
    "Test-compile a string.  Return the status and the compilation time"
    here = os.getcwd()
    try:
        there = tempfile.mkdtemp()
        os.chdir(there)
        test = "test" + source_extension
        with open(test, "w", encoding="ascii", errors="surrogateescape") as mdst:
            mdst.write(source)
            mdst.flush()
        buildit = compile_command + test
        if debug >= COMMAND_DEBUG:
            sys.stdout.write(buildit + "\n")
        if debug >= COMPILE_DEBUG:
            sys.stdout.write(source)
        result = subprocess.run(
            buildit.split(),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=False,
        )
        # output = result.stdout.decode()
        errors = result.stderr.decode()
        if debug >= COMPILE_DEBUG and errors:
            sys.stdout.write(errors)
        status = result.returncode
        if status == 0 and capture:
            return subprocess.getstatusoutput("./a.out")
        if debug >= COMPILE_DEBUG:
            sys.stdout.write("\n")
    finally:
        shutil.rmtree(there)
        os.chdir(here)
    return status == 0


def gencode(includes, snippets):
    "Generate a C test program from includes and a code snippet,"
    # if debug > 0:
    #    print("gencode(includes=%s snippets=%s)" % (repr(includes), snippets))
    code = "".join(f"#define {fn}\n" for fn in includes if "." not in fn)
    code += "".join(f"#include <{fn}>\n" for fn in includes if "." in fn)
    code += "int main(int argc, char *argv) {\n"
    code += "".join(snippets)
    code += "}\n"
    return code


def macroize(name):
    "Convert a feature name to a guard macro."
    strout = ""
    for c in name:
        if c == "*":
            strout += "P"
        elif c in string.punctuation or c == " ":
            strout += "_"
        elif c.isalpha():
            strout += c.upper()
        else:
            strout += c
    return strout


##
## Functions safe for use in scripts end here
##
## Others are not guaranteed to have stable interfaces
## or implementations.
##


def check_have(includes, others):
    "Perform HAVE checks for features"
    if debug >= TRACE_DEBUG:
        print(f"check_have(includes={includes} others={others})")
    snippets = []
    for (i, name) in enumerate(others):
        if name.endswith("_t"):
            snippets.append(f"    {name} g{i};\n")
        else:
            snippets.append(f"    {name};\n")
    code = gencode(includes, snippets)
    defs = []
    if testcompile(code):
        for arg in includes + others:
            defs.append(("-DHAVE_" + macroize(arg), ""))
    elif undefines:
        for arg in includes + others:
            defs.append(("-UHAVE_" + macroize(arg), ""))
    return defs


def check_decl(includes, others):
    "Perform HAVE_DECL checks for defined macros"
    if debug >= TRACE_DEBUG:
        print(f"check_have(includes={includes} other={others})")
    snippets = []
    for name in others:
        snippets.append(f"    {name};\n")
    code = gencode(includes, snippets)
    defs = []
    if testcompile(code):
        for arg in includes:
            defs.append(("-DHAVE_" + macroize(arg), ""))
        for arg in others:
            defs.append(("-DHAVE_DECL_" + macroize(arg), ""))
    elif undefines:
        for arg in includes:
            defs.append(("-UHAVE_" + macroize(arg), ""))
        for arg in others:
            defs.append(("-UHAVE_DECL_" + macroize(arg), ""))
    return defs


def check_lib(includes, others):
    "Perform LIB check for a library"
    if debug >= TRACE_DEBUG:
        print(f"check_lib(includes={includes} others={others})")
    if not others:
        sys.stderr.write("configure: CHECK_LIB requires at least one argument.\n")
        raise SystemExit(1)
    libname = others.pop(0)
    snippets = []
    for name in enumerate(others):
        snippets.append(f"    {name};\n")
    code = gencode(includes, snippets)
    defs = []
    if testcompile(code):
        defs.append(("-DHAVE_LIB" + macroize(libname), ""))
        defs.append(("-l" + libname, ""))
        for arg in others:
            defs.append(("-DHAVE_" + macroize(arg), ""))
    elif undefines:
        defs.append(("-UHAVE_LIB" + macroize(libname), ""))
        for arg in others:
            defs.append(("-UHAVE_" + macroize(arg), ""))
    return defs


def check_record(includes, others, rtype):
    "Perform STRUCT or UNION check for a structure and members"
    if debug >= TRACE_DEBUG:
        print(f"check_record(includes={includes} others={others} rtype={rtype})")
    if not others:
        sys.stderr.write(f"configure: {rtype} requires at least one argument.\n")
        raise SystemExit(1)
    rname = others.pop(0)
    snippets = []
    snippets.append(f"    {rname} x;\n")
    for name in others:
        snippets.append(f'    printf("%zd\\n",sizeof(x.{name}));\n')
    code = gencode(includes + ["stdio.h"], snippets)
    defs = []
    if testcompile(code):
        defs.append(("-DHAVE_" + macroize(rname), ""))
        for arg in others:
            defs.append(
                (
                    "-DHAVE_" + macroize(rname) + "_" + macroize(arg),
                    "",
                )
            )
    elif undefines:
        defs.append(("-UHAVE_" + macroize(rname), ""))
        for arg in others:
            defs.append(
                (
                    "-UHAVE_" + macroize(rname) + "_" + macroize(arg),
                    "",
                )
            )
    return defs


def check_sizeof(includes, others):
    "Perform sizeof discovery on a type"
    if debug >= TRACE_DEBUG:
        print(f"check_sizeof(includes={includes} other={others})")
    defs = []
    for typename in others:
        snippet = f'    printf("%zd\\n", sizeof({typename}));\n'
        code = gencode(includes + ["stdio.h"], snippet)
        res = testcompile(code, capture=True)
        if res:
            (_, output) = res
            defs.append(("-DSIZEOF_" + macroize(typename), output.strip()))
    return defs


def check_option(args, enables):
    "Process command line enables"
    if debug >= TRACE_DEBUG:
        print(f"check_option=(args=%{args}, enables={enables})")
    enable = args.pop(0)
    if helpmode:
        description = " ".join(args)
        if description.startswith('"'):
            description = description[1:]
        if description.endswith('"'):
            description = description[:-1]
        return [enable + ": " + description]
    if enable in enables:
        if "=" in enable:
            (k, v) = enable.split("=")
            return [("-DENABLE_" + macroize(k) + "=" + v, "")]
        return [("-DENABLE_" + macroize(enable), "")]
    if undefines:
        if "=" in enable:
            (k, v) = enable.split("=")
            return [("-UENABLE_" + macroize(k) + "=" + v, "")]
        return [("-UENABLE_" + macroize(enable), "")]
    return []


def check_words_bigendian():
    "Check endianness."
    if debug >= TRACE_DEBUG:
        print("check_wors_bigendian")
    code = """
/* Return status 1 if the machine is bigendian, 0 otherwise */
#include <stdlib.h>
#include <stdint.h>
int main(int argc, char *argv[]) {
    uintptr_t x = 0x01;
    char *p = (char *)&x;
    exit(*p == 0);
}
"""
    status = testcompile(code)
    if not status:
        return ("-DWORDS_BIGENDIAN", "")
    if undefines:
        return ("-UWORDS_BIGENDIAN", "")
    return []


def check_program(arguments):
    "Check for a specified proram being in $PATH and executable."
    defs = []
    for program_name in arguments:
        if any(
            os.path.isfile(os.path.join(path, program_name))
            and os.access(os.path.join(path, program_name), os.X_OK)
            for path in os.environ["PATH"].split(os.pathsep)
        ):
            defs.append(("-UHAVE_PROGRAM_" + macroize(program_name), ""))
    return defs


def check_script(arguments):
    "Run a Python script."
    defs = []
    for program_name in arguments:
        configdict = {"report": []}
        try:
            with open(
                program_name, "r", encoding="ascii", errors="surrogateescape"
            ) as msrc:
                exec(msrc.read(), configdict)
            defs.extend(configdict["report"])
        except (OSError, IOError) as exc:
            sys.stderr.write(f"{program_name}: in CHECK_SCRIPT, file or OS error!\n")
            raise SystemExit(1) from exc
        # We choose not to catch execution errors - let the user see the stack trace.
    return defs


install_keys = [
    "srcdir",
    "prefix",
    "exec-prefix",
    "bindir",
    "sbindir",
    "libexecdir",
    "sysconfdir",
    "sharedstatedir",
    "localstatedir",
    "libdir",
    "includedir",
    "oldincludedir",
    "datarootdir",
    "datadir",
    "infodir",
    "localedir",
    "mandir",
    "docdir",
    "htmldir",
    "dvidir",
    "pdfdir",
    "psdir",
]

if __name__ == "__main__":
    try:
        (opts, arguments) = getopt.getopt(
            sys.argv[1:],
            "df:o:tu",
            [
                "enable=",
                "help",
            ]
            + [(d + "=") for d in install_keys],
        )
    except getopt.GetoptError:
        sys.stderr.write(__doc__)
        # pylint: disable=raise-missing-from
        raise SystemExit(1)
    try:
        helpmode = False
        idheader = True
        undefines = False
        infile = "Makefile"
        outfile = "config.mk"
        debug = 0
        enables = []
        config_h = None
        pathdict = {}

        for (opt, arg) in opts:
            if opt == "-d":
                debug += 1
            elif opt == "-f":
                infile = arg
                if infile == "-":
                    infile = "/dev/stdin"
            elif opt == "-o":
                outfile = arg
                if outfile == "-":
                    outfile = "/dev/stdout"
            elif opt == "-t":
                infile = "/dev/stdin"
                outfile = "/dev/stdout"
                idheader = False
            elif opt == "-u":
                undefines = True
            elif opt == "--enable":
                enables.append(arg)
            elif opt == "--help":
                sys.stdout.write(__doc__)
                helpmode = True
            else:
                for option in install_keys:
                    if opt == "--" + option:
                        pathdict[option] = arg

        includable = re.compile("[.]|_.+?_SOURCE")
        results = []
        try:
            with open(infile, "r", encoding="ascii", errors="surrogateescape") as msrc:
                for (n, line) in enumerate(msrc):
                    m = re.match(r"#\s*CHECK_(\w+)\(([^)]+)\)", line)
                    if m:
                        if debug >= ECHO_DEBUG:
                            sys.stdout.write(line)
                        test = m.group(1)
                        arguments = m.group(2)
                        raw_arguments = [
                            arg.strip().replace(",", "")
                            for arg in shlex.split(arguments)
                            if arg != ","
                        ]
                        arguments = []
                        while raw_arguments:
                            nxt = raw_arguments.pop(0)
                            if nxt in ("struct", "union") and raw_arguments:
                                nxt += " " + raw_arguments.pop(0)
                            while raw_arguments and raw_arguments[0] == "*":
                                nxt += " " + raw_arguments.pop(0)
                            arguments.append(nxt)
                        includes = [arg for arg in arguments if includable.search(arg)]
                        others = [
                            arg for arg in arguments if not includable.search(arg)
                        ]
                        try:
                            if test == "HAVE":
                                found = check_have(includes, others)
                            elif test == "DECL":
                                found = check_decl(includes, others)
                            elif test == "LIB":
                                found = check_lib(includes, others)
                            elif test == "STRUCT":
                                found = check_record(includes, others, "STRUCT")
                            elif test == "UNION":
                                found = check_record(includes, others, "UNION")
                            elif test == "SIZEOF":
                                found = check_sizeof(includes, others)
                            elif test == "OPTION":
                                found = check_option(arguments, enables)
                            elif test == "PROGRAM":
                                found = check_program(arguments)
                            elif test == "SCRIPT":
                                found = check_script(arguments)
                            elif test == "CONFIG":
                                config_h = arguments[0]
                                continue
                            else:
                                sys.stderr.write(
                                    f"{msrc.name}:{n+1}: unknown test {test}\n"
                                )
                                continue
                        except (OSError, IOError) as exc:
                            sys.stderr.write("f{msrc.name}:{n+1}: file or OS error!\n")
                            raise SystemExit(1) from exc
                        # This is how we make it harmless to test for the same feature
                        # multiple times.
                        for pair in found:
                            if pair not in results:
                                results.append(pair)
                    elif line.startswith("# ") and "CHECK_WORDS_BIGENDIAN" in line:
                        found = check_words_bigendian()
                        if found and found not in results:
                            results.append(found)
                    elif line.startswith("# ") and "CHECK" in line:
                        sys.stderr.write(f"{msrc.name}:{n+1}: possibly garbled test.\n")
        except (OSError, IOError) as exc:
            sys.stderr.write(f"{infile}: file or OS error opening input file\n")
            raise SystemExit(1) from exc
        try:
            with open(outfile, "w", encoding="ascii", errors="surrogateescape") as mdst:
                if helpmode:
                    if results:
                        mdst.write("\nConfiguration options:\n")
                    for text in results:
                        mdst.write(text + "\n")
                else:
                    if idheader:
                        # pylint: disable=consider-using-f-string
                        mdst.write("# Machine: {0}\n".format(platform.machine()))
                    for (sym, val) in results:
                        if sym.startswith("-D") and val:
                            if not config_h:
                                mdst.write(f"CFLAGS += {sym}={val}\n")
                        elif sym.startswith("-D") or sym.startswith("-U"):
                            if not config_h:
                                mdst.write(f"CFLAGS += {sym}\n")
                        elif sym.startswith("-l"):
                            mdst.write(f"LDFLAGS += {sym}\n")
                        else:
                            mdst.write(f"{sym} = {val}\n")
                    for option in install_keys:
                        if option in pathdict:
                            mdst.write(
                                option.replace("-", "_")
                                + " = "
                                + pathdict[option]
                                + "\n"
                            )
                    if config_h:
                        mdst.flush()
                        with open(
                            config_h, "w", encoding="ascii", errors="surrogateescape"
                        ) as cdst:
                            for (sym, val) in results:
                                if sym.startswith("-D") and val:
                                    cdst.write(f"#define {sym[2:]} {val}\n")
                                elif sym.startswith("-D"):
                                    cdst.write(f"#define {sym[2:]}\n")
        except (OSError, IOError) as exc:
            sys.stderr.write(f"{outfile}: file or OS error opening output file\n")
            raise SystemExit(1) from exc

    except KeyboardInterrupt as exc:
        sys.stderr.write("configure: interrupted, bailing out!\n")
        raise SystemExit(1) from exc
    except BrokenPipeError as exc:
        sys.stderr.write("configure: broken-pipe error, bailing out!\n")
        raise SystemExit(1) from exc
# end
