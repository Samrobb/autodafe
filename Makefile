# Project makefile for autodafe
#
# Requires Python 3 and asciidoctor. Tesrts also want pylint
#
# makemake.svg is from https://en.wikipedia.org/wiki/Makemake#/media/File:Makemake_symbol_(bold).svg
# Required attribution: By PlanetUser - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=45820499

VERSION=$(shell sed -n <NEWS.adoc '/::/s/^\([0-9][^:]*\).*/\1/p' | head -1)

SOURCES = README.adoc COPYING NEWS.adoc de-autoconfiscation.adoc Makefile \
	makemake makemake.adoc deconfig deconfig.adoc configure configure.adoc \
	control TODO.adoc makemake-logo.png

.SUFFIXES: .html .adoc .1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

all: makemake.html deconfig.html de-autoconfiscation.html

clean:
	rm -f ascii ascii.o splashscreen.h nametable.h
	rm -f *.rpm *.tar.gz MANIFEST *.1 *.html

prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

install: makemake.1
	install -d "$(target)/bin"
	install -d "$(target)/$(mandir)/man1"
	install -m 755 makemake "$(target)/bin"
	install -m 644 makemake.1 "$(target)/$(mandir)/man1"
	if [ -O makemake.1 ]; then rm makemake.1; fi

uninstall:
	-rm "$(target)/$(mandir)/man1/makemake.1"
	-rm "$(target)/bin/makemake"
	-rmdir -p "$(target)/$(mandir)/man1"
	-rmdir -p "$(target)/bin"

reflow:
	@black -q makemake

pylint:
	@pylint --score=n makemake
	@pylint --score=n deconfig
	@pylint --score=n configure

shellcheck:
	@-shellcheck -f gcc tests/*.sh tests/test_makemake tests/test_deconfig

check: pylint shellcheck
	cd tests; $(MAKE) --quiet

# Don't do pylint on Gitlab, there's a version-skew problem.
cicheck:
	cd tests; $(MAKE) --quiet

fixme:
	@if command -v rg >/dev/null; then \
		rg --no-heading --iglob '!tests ' "[#*] FIX""ME"; \
	else \
		find . -type f -exec grep -n '[#*] FIXME' {} /dev/null \; | grep -v "[.]git"; \
	fi
version:
	@echo $(VERSION)

EXTRA = makemake.1 deconfig.1 configure.1
autodafe-$(VERSION).tar.gz: $(SOURCES) $(EXTRA)
	@(git ls-files; ls *.1) | sed s:^:autodafe-$(VERSION)/: >MANIFEST
	@(cd ..; ln -s autodafe autodafe-$(VERSION))
	(cd ..; tar -czf autodafe/autodafe-$(VERSION).tar.gz `cat autodafe/MANIFEST`)
	@ls -l autodafe-$(VERSION).tar.gz
	@(cd ..; rm autodafe-$(VERSION))

dist: autodafe-$(VERSION).tar.gz

release: autodafe-$(VERSION).tar.gz makemake.html hacking.html de-autoconfiscation.html
	shipper version=$(VERSION) | sh -e -x

refresh: makemake.html
	shipper -N -w version=$(VERSION) | sh -e -x

ifeq ($(shell uname),Limux)
os:
	@echo $(shell uname)
endif
