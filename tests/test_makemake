#! /bin/sh
## Test Makefile conversion 
#
# test_makemake [-d] [-h] [-s phase] [-p opts] [-u] [-v] project
#
# The test harness for makemake.  The default behavior is to
# cd into the directory ${project}-project and test makemake
# there.  The project is configured and built, then makemake
# is applied to it and the build done with the dobfuscated makefile.
#
# Options:
#    -d: dryrun, echo commands but don't run them
#    -h: delete config.h and config.h to test include generation 
#    -p: pass otions to makemake
#    -s: stop after specified phase
#    -u: test undo action instead of transform and revert
#    -v: don't suppress command and message output
#
# Various sanity checks are done throughout.  Note: the sanity checks
# currently assumes that every subdirectory has its own Makefile
#
# Output is in TAP.

# shellcheck disable=SC1091
. ./common-setup.sh

msgsink=/dev/null
clobber=np
stopafter="end"
run=""
mode="normal"
makeopts=""
pass_opts=""
while getopts dhp:s:uv opt
do
    case $opt in
	d) run="echo"; msgsink=/dev/stdout;;
	h) clobber=yes;;
	s) stopafter=$OPTARG;;
	p) pass_opts="${pass_opts} $OPTARG";;
	u) mode="undo";;
	v) msgsink=/dev/stdout;;
	*) echo "$0: unknown flag $opt" >&2; exit 1;;
    esac
done
# shellcheck disable=SC2004
shift $(($OPTIND - 1))

if [ "$stopafter" != "" ]
then
    case "$stopafter" in
	autoreconf|configure|make|generation|patching|clobber|makemake|check|clean|remake|reclean|end) ;;
	*) echo "$0: unknown stop phase $stopafter"; exit 1 ;;
    esac
fi

check_libtool() {
    [ -f libtool ] && echo "libtool exists."  >"${msgsink}" 2>&1
}

# Make it possible to specify by either stem or fullname
stem=${stem%%-project}

stem="$1"
if [ "$stem" = "" ]
then
    echo "not ok - no project specified"
    exit 1
fi

if [ "$stopafter" != "end" ]
then
    builddir="testbuild"
    msgsink=/dev/stdout
else
    builddir="/tmp/${stem}$$"
    trap 'rm -fr $builddir' EXIT HUP INT QUIT TERM
fi

rm -fr "$builddir"
# shellcheck disable=SC2086
$run cp -r ${stem}-project $builddir >"${msgsink}" 2>&1
tapout "${stem} directory copy"

# shellcheck disable=SC2086
$run tapcd "$builddir"

# shellcheck disable=SC2086
$run autoreconf --install  >"${msgsink}" 2>&1
tapout "${stem} autoreconf"

if [ "$stopafter" = "autoreconf" ]
then
    exit 0
fi

check_libtool

printf "\n*** About to configure...\n" >"${msgsink}" 2>&1
# shellcheck disable=SC2086
$run ./configure >"${msgsink}" 2>&1
tapout "${stem} configure"

if [ "$stopafter" = "configure" ]
then
    exit 0
fi

check_libtool

if [ "$mode" = "normal" ]
then
    printf "\n*** About to make...\n" >"${msgsink}" 2>&1
    # shellcheck disable=SC2086
    $run make ${makeopts} >"${msgsink}" 2>&1
    tapout "${stem} regular make"

    if [ "$stopafter" = "make" ]
    then
	exit 0
    fi
fi


printf "\n*** About to generate the patch...\n" >"${msgsink}" 
# shellcheck disable=SC2086
$run deconfig >lift.patch 2>"${msgsink}"

if [ "$stopafter" = "generation" ]
then
    exit 0
fi

printf "\n*** About to patch with deconfig output...\n" >"${msgsink}" 
# shellcheck disable=SC2086
$run patch -p1 <lift.patch >"${msgsink}" 2>&1

if [ "$stopafter" = "patching" ]
then
    exit 0
fi

if [ "$clobber" = yes ]
then
    printf "\n*** Removing config.h and config.h.in...\n" >"${msgsink}"

    rm -f config.h config.h.in
    if [ "$stopafter" = "clobber" ]
    then
	exit 0
    fi
fi

printf "\n*** About to 'makemake %s .'...\n" >"${msgsink}" "$pass_opts" 2>&1
# shellcheck disable=SC2086
$run makemake $pass_opts . >"${msgsink}" 2>&1
tapout "${stem} makemake ${pass_opts}"

if [ "$stopafter" = "makemake" ]
then
    exit 0
fi

if [ "$run" != "echo" ]
then
    printf "\n*** About to check Makefile...\n" >"${msgsink}" 2>&1
    # shellcheck disable=SC2044
    for dir in $(find . -type d -not -path '*/\.*')
    do
	if [ "$dir" = "./autom4te.cache" ] || [ "$dir" = "./m4" ]
	then
	    continue
	fi
	if [ ! -f "${dir}/Makefile.bak" ]
	then
	    echo "not ok - ${dir}/Makefile.bak not created"
	    exit 1
	fi
	# shellcheck disable=SC2086
	$run cmp "${dir}/Makefile" "${dir}/Makefile.bak" >"${msgsink}" 2>&1
	if [ $? = 9 ] 
	then
	    echo "not ok - original and de-obfuscated Makefiles in ${dir} are identical"
	    exit 1
	fi
    done
fi

if grep LIBTOOL Makefile.bak >/dev/null 2>&1
then
    printf "\n*** Checking library files after initial make...\n" >"${msgsink}" 2>&1
    # shellcheck disable=SC2035
    file *.so* *.la* >"${msgsink}" 2>&1
fi

if [ "$stopafter" = "check" ]
then
    exit 0
fi

printf "\n*** About to clean with deobfuscated Makefile...\n" >"${msgsink}" 2>&1
# shellcheck disable=SC2086
$run make ${makeopts} clean >"${msgsink}" 2>&1
tapout "de-obfuscated clean"

# shellcheck disable=SC2035
rm -f *.la

if [ "$stopafter" = "clean" ]
then
    exit 0
fi

if [ "$mode" = "normal" ]
then
     printf "\n*** About to make with deobfuscated Makefile...\n" >"${msgsink}" 2>&1
     # shellcheck disable=SC2086
     $run make ${makeopts} >"${msgsink}" 2>&1
     tapout "${stem} deobfuscated make"

     if grep LIBTOOL Makefile.bak >/dev/null 2>&1
     then
	 printf "\n*** Checking library files after remake...\n" >"${msgsink}" 2>&1
	 # shellcheck disable=SC2035
	 file *so* *.la* >"${msgsink}" 2>&1
     fi

     if [ "$stopafter" = "remake" ]
     then
	 exit 0
     fi

     printf "\n*** About to makemake -c...\n" >"${msgsink}" 2>&1
     # shellcheck disable=SC2086
     $run makemake -c . >"${msgsink}" 2>&1
     tapout "${stem} makemake -c"

     if [ "$stopafter" = "reclean" ]
     then
	 exit 0
     fi

     if [ -f Makefile.bak ]
     then
	 echo "not ok - makemake -c failed to delete Makefile.bak"
	 exit 1
     fi
else
     printf "\n*** About to makemake -u...\n" >"${msgsink}" 2>&1
     $run makemake -u . >"${msgsink}" 2>&1
     tapout "${stem} mamake -u"

    if [ "$run" != "echo" ]
    then
	printf "\n*** About to check Makefiles after makemake -u...\n" >"${msgsink}" 2>&1
	# shellcheck disable=SC2044
	for dir in $(find . -type d -not -path '*/\.*')
	do
	    if [ "$dir" = "./autom4te.cache" ]
	    then
		continue
	    fi
	    if [ -f "${dir}/Makefile.bak" ]
	    then
		echo "not ok - ${dir}/Makefile.bak is ureverted"
		exit 1
	    fi
	    if [ ! -f "${dir}/Makefile" ]
	    then
		echo "not ok - ${dir}/Makefile was expected but does not exist"
		exit 1
	    fi
	done
    fi
fi

printf "\n*** Done.\n" >"${msgsink}" 2>&1

echo "ok - ${stem} ${mode} test succeeded."

if [ "$stopafter" != "end" ] && [ "$run" = "" ]
then
   echo "${stem}: build results left in ${builddir}"
fi

# end

