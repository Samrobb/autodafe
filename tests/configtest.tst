# This has a cfg extension so it won't get swept up in the makemake tesst
#options: --enable=foo --bindir=/usr/fuzzlebin

# CHECK_OPTION(foo, "Example foo option, you should see a -D for this")

# CHECK_OPTION(bar, "Example bar option, you should not see a -D for this")

# CHECK_HAVE(gardafui.h)

# CHECK_HAVE(stdio.h, fputs)

# CHECK_HAVE(stdio.h, gronk)

# CHECK_DECL(stdio.h, BUFSIZ)

# CHECK_DECL(stdio.h, WRONGWAY)

# CHECK_LIB(math.h, m)

# CHECK_LIB(frogger.h, f)

# CHECK_STRUCT(time.h, struct timespec, tv_sec)

# CHECK_STRUCT(time.h, struct nosuch, noway)

# CHECK_SIZEOF(char)

# CHECK_PROGRAM(sh)

# CHECK_PROGRAM(impossible-i-say)

# CHECK_SCRIPT(scriptdemo)

# These are architecture-dependent
## CHECK_SIZEOF(stdint.h, int32_t)
## CHECK_SIZEOF(termios.h, struct termios)
## CHECK_WORDS_BIGENDIAN
