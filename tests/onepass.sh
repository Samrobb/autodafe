#! /bin/sh
## check that mmake has one-pass behavior

# shellcheck disable=SC1091
. ./common-setup.sh

trap 'rm -f /tmp/twopass$$.mk' EXIT HUP INT QUIT TERM

# It will be extremely surprising if this ever fails.
cat >/tmp/twopass$$.mk <<EOF
FOO="before"
foo:
	@echo "Checking" "${FOO}" "${BAR}"
BAR="after"
EOF

tapcd /tmp
make -f twopass$$.mk foo | grep -v after >/dev/null
tapcheck "make forward reference test"
